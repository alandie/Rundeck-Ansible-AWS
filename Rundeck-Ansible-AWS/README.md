# Deploy in Amazon AWS with Ansible and Rundeck
  
In this sample project we deploy the following in AWS, thanks to the provided Ansible playbooks:
  
- **AWS Infrastructure**:  create VPC, subnet, Internet Gateway, Security Group
- **Database Server (MySQL)**: launch an EC2 instance and configure a MySQL database with the expected schema
- **Web Application Server (Tomcat)**: launch an Elastic Load Balancer and 2 EC2 instances attached to this ELB, install Tomcat with the .WAR archive of our sample web application
  
Rundeck will be configured in the last part, in order to provide a Web Interface with the following features: 
- Access control and logging capabilities
- Job scheduling
- Execution of Ansible playbooks through Rundeck jobs
- Ability to sort and filter the AWS instances, reported by the Ansible inventory
- Ability to execute some remote commands onto the AWS instances  
   
## Prerequisistes
  
- Install Python and Ansible
- Copy *scripts/ec2.py* and *scripts/ec2.ini" (script which will manage the hosts inventory in AWS for Ansible)
- Add both following environment variables related to your AWS account: *AWS_ACCESS_KEY_ID* and *AWS_SECRET_ACCESS_KEY*
  
## Installation ##
- Copy the whole structure in *ansible* folder 
- In *ansible/group_vars/all/vars_file.yml*, customize the variables values
- In *ansible/roles/infrastructure/vars/main.yml*, customize the variables values
  
## Deployment ##
- Deploy the AWS infrastructure: *ansible-playbook create-infrastructure.yml -v*
- Launch the EC2 instance for the MySQL server: *ansible-playbook launchDBServer.yml -v*
- Configure MySQL Database: *ansible-playbook configureMySqlDB.yml -v*
- Launch ELB and the 2 EC2 instances for the Web Servers: *ansible-playbook launchWebServers.yml -v*
- Configure the Web Servers (install Tomcat, install the application WAR file, set up DB access configuration): *ansible-playbook configureWebservers.yml -v*
  
![](img/RunningApp.png "The Application is now running")
  
## Change Management ##
- Install the latest packages on the Web Servers: *ansible-playbook update-system.yml -v*
- Update the WAR file on the Tomcat Web Servers: *ansible-playbook update-application.yml -v*
  
## Terminate ##
- Terminate the Elastic Load Balancer and the Web Server instances: *ansible-playbook terminate-Webservers.yml -v*
- Terminate the Database Server: *ansible-playbook terminateDBServer.yml -v*
  
## Rundeck configuration ##
- Install Rundeck 
- Install the Ansible plugin for Rundeck
- Access Rundeck Web Interface
  
We will then create 2 Rundeck projects: 
  
### Rundeck Project for executing the Ansible Playbooks: ###
  
This project will only target the local Node (local execution of the Ansible Playbooks)
  
- Create a simple Rundeck project
- Into this project create one Job for each Ansible playbook:
  
![](img/Rundeck2.png "Example of a job definition for calling an Ansible Playbook")
  
- You can also create a workflow for sequentially executing several ansible playbooks (for example for setting up our complete Web Application):
   
![](img/Rundeck8.PNG "Example of Workflow for calling several Ansible Playbooks in sequence")
  
- At the end, you should have the following available Jobs:
  
![](img/Rundeck1.PNG "Rundeck Jobs for the Ansible Playbooks")
  
- We can now run or schedule these jobs: 
  
![](img/Rundeck3.PNG "Running a Rundeck job")
  
- We can monitor the Job progress and get its log report:
  
![](img/Rundeck5.PNG "Monitoring the execution of a Rundeck job")
  
### Rundeck Project for managing the EC2 instances: ### 
  
This project will rely onto the Ansible inventory for AWS (ec2.py). It will give us the opportunity to list, search, filter and execute some remote commands (SSH) onto our EC2 instances.
  
- Create a Rundeck project, which relies onto the Ansible Plugin. The inventory configuration must target your script *ec2.py*. 
  
![](img/Rundeck20.PNG "Configuring Rundeck project for accessing EC2 instances")
  
- When you then go into the Nodes section, you should be able to search and filter your EC2 instances (many available search criteria)
  
![](img/Rundeck17.PNG "Search EC2 instances")
![](img/Rundeck19.PNG "Filter EC2 instances")
  
- You can then remotely run some commands on some of the EC2 instances: 
  
![](img/Rundeck22.PNG "Run command on EC2 instances")
  