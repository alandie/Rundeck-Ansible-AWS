DROP TABLE IF EXISTS users;
CREATE TABLE users(userid INT NOT NULL AUTO_INCREMENT, firstName VARCHAR(100) NOT NULL, lastName VARCHAR(100) NOT NULL, dob DATE, email VARCHAR(100), PRIMARY KEY ( userid ));
INSERT INTO users (firstName, lastName, dob, email) VALUES ('John','Smith',NOW(),'john.smith@mycompany.net');
INSERT INTO users (firstName, lastName, dob, email) VALUES ('Bob','Walters',NOW(),'bob.walters@mycompany.net');
INSERT INTO users (firstName, lastName, dob, email) VALUES ('Claire','Johnson',NOW(),'claire.johnson@mycompany.net');
